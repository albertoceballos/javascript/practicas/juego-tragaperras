window.addEventListener("load", function (){
	var monedas=0;
	var apuesta=1;
	//precargar las imágenes de las frutas en un array de obejetos de tipo imagen con sus rutas
	var frutas=new Array();

	var aleatorio=[];
	for(i=0;i<8;i++){
		frutas[i]=new Image();
		frutas[i].src="img/frutas/"+(i+1)+".svg";
	}
	//Array con los divs de las frutas
	var imgFrutas=document.querySelectorAll(".imgFrutas");
	
	//Calcular un número aleatorio entre 1 y el número de imágenes y asignarle
	//ese número del array de imágenes a los divs

	for(i=0;i<3;i++){
		aleatorio[i]=Math.floor((Math.random()*frutas.length-1)+1);
		imgFrutas[i].style.backgroundImage="url("+frutas[aleatorio[i]].src+")";
	}

	function tirada(){
		var cerezas=0;
		var iguales=0;
		var ganar=false;
		var premio=0;
		//Capturar apuesta
			apuesta=parseInt(document.querySelector("#textApuestas").value);
			console.log('APUESTA:'+apuesta);
		//volver a recargar las imágenes con números aleatorios
		for(i=0;i<3;i++){
			aleatorio[i]=Math.floor((Math.random()*frutas.length-1)+1);
			if(aleatorio[i]==0){
				cerezas++;
			}
			console.log("cerezas:"+cerezas);
			imgFrutas[i].style.backgroundImage="url("+frutas[aleatorio[i]].src+")";
		}
		//comprobar el número de cerezas sacadas
		if(cerezas==1){
			premio+=1;
		}else if(cerezas==2){
			premio+=4;
		}else if(cerezas==3){
			premio+=10;
		}

		//Comparar para saber si hay frutas repetidas:
		for(i=0;i<aleatorio.length;i++){
        	for(var j = i; j <= aleatorio.length; j++) {
              	if(i != j && aleatorio[i] == aleatorio[j]) {
             		//si son iguales, iguales +1;
                	iguales++;
                	//si hay dos frutas iguales:
					if(iguales==1){
					//si hay dos iguales pero no son cerezas, se ganan 2 monedas
                		if(aleatorio[i]!=0 && aleatorio[j]!=0){
                			ganar=true;
                			premio+=2;
                			console.log('2 frutas iguales que NO SON CEREZAS');
               		 	}
					}
					if(iguales==3){
					//si hay tres frutas iguales pero no son cerezas, se ganan 5 monedas(2 más)
						if(aleatorio[i]!=0){
							ganar=true;
							premio+=3;
							console.log('3 frutas iguales que NO SON CEREZAS');
						}
					}
              	}
       	    }
		}
		if(cerezas>0){
			ganar=true;
		}
		console.log(ganar);
		//si se ha ganado poner emoji contento si se ha perdido el emoji triste
		if(ganar==true){
			document.querySelector("#premios").style.backgroundImage = "url(img/face-smile.svg)";
		}else{
			document.querySelector("#premios").style.backgroundImage = "url(img/face-sad.svg)";
		}

		premio=premio*apuesta;
		console.log(premio);
		monedas+=premio;
		document.querySelector("#textMonedas").value=monedas;
		document.querySelector("#premios").value=premio;
		
	}

	//Función para el botón de meter monedas
	document.querySelector("#meterMoneda").addEventListener("click", ()=>{
		monedas++;
		document.querySelector("#textMonedas").value=monedas;
	});

	//Función para el botón de aumentar apuesta
	document.querySelector("#btnApuesta").addEventListener("click",()=>{
		if(monedas>0){
			document.querySelector("#textApuestas").value=parseInt(document.querySelector("#textApuestas").value)+1;
			monedas--;
			document.querySelector("#textMonedas").value=monedas;
		}

	});

	//Función del botón de jugar

	document.querySelector("#jugar").addEventListener("click",()=>{
		if(monedas>0){
			monedas=monedas-apuesta;
			console.log("monedas: "+monedas);
			document.querySelector("#textMonedas").value=monedas;
			tirada();
		}
	});
});